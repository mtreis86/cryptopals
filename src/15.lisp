(in-package :cryptopals)



;;;PKCS#7 padding validation
#|Write a function that takes a plaintext, determines if it has valid PKCS#7 padding, and strips the
padding off.

The string:

"ICE ICE BABY\x04\x04\x04\x04"

... has valid padding, and produces the result "ICE ICE BABY".

The string:

"ICE ICE BABY\x05\x05\x05\x05"

... does not have valid padding, nor does:

"ICE ICE BABY\x01\x02\x03\x04"

If you are writing in a language with exceptions, like Python or Ruby, make your function throw an
exception on bad padding.
Crypto nerds know where we're going with this. Bear with us.|#

(defun unpad-string-pkcs7 (string)
  (unpad-vect-pkcs7 (byte-vect-to-string string)))

(defun ex15 ()
  (let ((icey "ICE ICE BABY"))
    (and (equalp (unpad-vect-pkcs7 #(73 67 69 32 73 67 69 32 66 65 66 89 4 4 4 4))
                 (string-to-byte-vect icey))
         (not (valid-pkcs7 #(73 67 69 32 73 67 69 32 66 65 66 89 5 5 5 5)))
         (not (valid-pkcs7 #(73 67 69 32 73 67 69 32 66 65 66 89 1 2 3 4))))))

