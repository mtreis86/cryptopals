(in-package :cryptopals)




;;; Implement CBC mode
#| CBC mode is a block cipher mode that allows us to encrypt irregularly-sized messages, despite the
fact that a block cipher natively only transforms individual blocks. In CBC mode, each ciphertext
block is added to the next plaintext block before the next call to the cipher core. The first
plaintext block, which has no associated previous ciphertext block, is added to a "fake 0th
ciphertext block" called the initialization vector, or IV.
Implement CBC mode by hand by taking the ECB function you wrote earlier, making it encrypt instead
of decrypt (verify this by decrypting whatever you encrypt to test), and using your XOR function
from the previous exercise to combine them.
The file here is intelligible (somewhat) when CBC decrypted against "YELLOW SUBMARINE" with an IV of
all ASCII 0 (\x00\x00\x00 &c) Don't cheat.|#

(defun ex10 ()
  (let* ((key "YELLOW SUBMARINE")
         (init-vector (make-byte-vect 16))
         (path #P"~/common-lisp/puzzles/cryptopals/encrypted-files/10.txt")
         (desired-output *funky-music*)
         (calculated-output (decrypt-b64-file-aes-cbc key init-vector path)))
     (string= desired-output calculated-output)))
