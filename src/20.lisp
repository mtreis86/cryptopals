(defpackage ex20
  (:use :cl
        :cryptutils/prng
        :bit-foolery))

(in-package :cryptopals)




;;;Break fixed-nonce CTR statistically

#| In this file find a similar set of Base64'd plaintext. Do with them exactly what you did with the
first, but solve the problem differently.

Instead of making spot guesses at to known plaintext, treat the collection of ciphertexts the same
way you would repeating-key XOR.

Obviously, CTR encryption appears different from repeated-key XOR, but with a fixed nonce they are
effectively the same thing.

To exploit this: take your collection of ciphertexts and truncate them to a common length the
length of the smallest ciphertext will work.

Solve the resulting concatenation of ciphertexts as if for repeating- key XOR, with a key size of
the length of the ciphertext you XOR'd. |#

;; TODO this mostly works but the first character of each result is wrong
;; also it doesn't return a boolean

(defun ex20 ()
  (let* ((path #P"~/common-lisp/puzzles/cryptopals/encrypted-files/20.txt")
         (b64-vects (read-b64-file-to-byte-vect path))
         (min (loop for b64 in b64-vects minimizing (length b64)))
         (vects (loop for b64 in b64-vects
                      collecting (subseq b64 0 min)))
         (key (attempt-decode-xor-repeating-key-vect-list vects)))
    (loop for vect in vects
          do (print (byte-vect-to-string (byte-vect-xor vect key))))))




