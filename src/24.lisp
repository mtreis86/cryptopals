(defpackage ex24
  (:use :cl
        :cryptutils/prng
        :bit-foolery))

(in-package :ex24)

;;;; Create the MT19937 stream cipher and break it

#|You can create a trivial stream cipher out of any PRNG; use it to generate a sequence of 8 bit
outputs and call those outputs a keystream. XOR each byte of plaintext with each successive byte
of keystream.

Write the function that does this for MT19937 using a 16-bit seed. Verify that you can encrypt and
decrypt properly. This code should look similar to your CTR code.

Use your function to encrypt a known plain text (say, 14 consecutive 'A' characters) prefixed by a
random number of random characters.

From the ciphertext, recover the "key" (the 16 bit seed).

Use the same idea to generate a random "password reset token" using MT19937 seeded from the
current time.

Write a function to check if any given password token is actually the product of an MT19937 PRNG
seeded with the current time.|#


(defun ex24 ()
 (set-seed 100)
 (let* ((test-string (coerce (append (loop repeat (+ 4 (random 14))
                                           collecting (random-alphanumeric))
                                     (loop repeat 14
                                           collecting #\A))
                             'string))
        (encrypted (encrypt test-string))
        (decrypted (decrypt encrypted)))
   (assert (string= test-string decrypted))
   (recover-key encrypted)))

(defun recover-key (crypttext))
  




(let (prng keys pointer seed)
  (defun set-seed (new-seed)
    (setf seed new-seed))
  (defun make-keystream ()
    (setf prng (seed-prng (make-prng seed))
          keys (make-array 4)
          pointer 0)
    (populate-keys)
    keys)
  (defun populate-keys ()
    (loop with from-prng = (extract-number prng)
          for index from 0 below 4
          for pos downfrom 24 to 0 by 8
          do (setf (aref keys index) (ldb (byte 8 pos) from-prng)))
    keys)
  (defun next-key ()
    (let ((result (aref keys pointer)))
      (if (= pointer 3)
          (progn (setf pointer 0)
                 (populate-keys))
          (incf pointer))
      result)))

(defun encrypt (plaintext)
  (loop initially (make-keystream)
        with length = (length plaintext)
        with encrypted = (make-array length :element-type '(integer 0 255))
        for index from 0 below length
        do (setf (aref encrypted index)
                 (logxor (char-code (aref plaintext index))
                         (next-key)))
        finally (return encrypted)))

(defun decrypt (crypttext)
  (loop initially (make-keystream)
        with length = (length crypttext)
        with decrypted = (make-string length)
       for index from 0 below length
       do (setf (char decrypted index)
                (code-char (logxor (aref crypttext index)
                                   (next-key))))
       finally (return decrypted)))
(defun random-alphanumeric ()
  (let ((rand (random 62)))
    (code-char (cond ((< rand 10)
                      (+ rand (char-code #\0)))
                     ((< rand 36)
                      (+ rand -10 (char-code #\a)))
                     (t (+ rand -36 (char-code #\A)))))))


