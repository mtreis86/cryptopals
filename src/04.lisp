(in-package :cryptopals)



;;; 4 Detect single-character XOR

#| One of the 60-character strings in this file, 4.txt, has been encrypted by single-character XOR.
Find it. (Your code from #3 should help.) |#

(defun ex04 ()
  (format t "Attempting decode of ex4 input.~%")
  (let ((desired-output "Now that the party is jumping
")
        (result (attempt-decode-xor-against-one-repeated-char-file
                 #P"~/common-lisp/puzzles/cryptopals/encrypted-files/4.txt")))
    (format t "Decoded input seems to be: ~A~%" (byte-vect-to-string result))
    (string= (byte-vect-to-string result) desired-output)))

