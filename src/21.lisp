(defpackage ex21
  (:use :cl
        :cryptutils/prng
        :bit-foolery))

(in-package :ex21)


;; TODO this only checks the initial array, we should probably make sure twisting also matches

(defun ex21 ()
  (let* ((seed 12345)
         (theirs (sb-kernel::init-random-state seed))
         (mine (seed-prng (make-prng seed))))
    (loop for test to 10
          collecting (= (aref theirs (+ test 4))
                        (aref (cryptutils/prng::prng-array mine) (+ test 1)))
          into results
          finally (return (every #'identity results)))))
