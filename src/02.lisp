(in-package :cryptopals)



;;; 2 Fixed XOR
#| Write a function that takes two equal-length buffers and produces their XOR combination.
If your function works properly, then when you feed it the string:
1c0111001f010100061a024b53535009181c
... after hex decoding, and when XOR'd against:
686974207468652062756c6c277320657965
... should produce:
746865206b696420646f6e277420706c6179
|#

(defun ex02 ()
  (let ((input1 "1c0111001f010100061a024b53535009181c")
        (input2 "686974207468652062756c6c277320657965")
        (desired-output "746865206b696420646f6e277420706c6179"))
    (string= desired-output
             (print-byte-vect-as-hex (byte-vect-xor (hex-string-to-byte-vect input1)
                                                    (hex-string-to-byte-vect input2))
                                     nil))))
