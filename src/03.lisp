(in-package :cryptopals)



;;; 3 Single-byte XOR cipher
#| The hex encoded string:
1b37373331363f78151b7f2b783431333d78397828372d363c78373e783a393b3736
... has been XOR'd against a single character. Find the key, decrypt the message.
You can do this by hand. But don't: write code to do it for you.
How? Devise some method for "scoring" a piece of English plaintext. Character frequency is a
good metric. Evaluate each output and choose the one with the best score. |#

(defun ex03 ()
  (let* ((input (string-upcase
                 "1b37373331363f78151b7f2b783431333d78397828372d363c78373e783a393b3736"))
         (desired-output "Cooking MC's like a pound of bacon")
         (calculated-output (multiple-value-bind (result key)
                                (attempt-decode-xor-against-one-repeated-char
                                  (hex-string-to-byte-vect input))
                             (declare (ignore key))
                             (byte-vect-to-string result))))
    (format t "Decoded string seems to be:~%~A~%" calculated-output)
    (string= desired-output calculated-output)))

