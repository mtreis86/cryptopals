(defpackage ex22
  (:use :cl
        :cryptutils/prng
        :bit-foolery))
        
(in-package :ex22)

;;;Crack an MT19937 seed

#|Make sure your MT19937 accepts an integer seed value. Test it - verify that you're getting the
same sequence of outputs given a seed.

Write a routine that performs the following operation:

Wait a random number of seconds between, I don't know, 40 and 1000.
Seeds the RNG with the current Unix timestamp
Waits a random number of seconds again.
Returns the first 32 bit output of the RNG.
You get the idea. Go get coffee while it runs. Or just simulate the passage of time, although
you're missing some of the fun of this exercise if you do that.

From the 32 bit RNG output, discover the seed.|#

(defvar *min-sleep* 40)
(defvar *max-sleep* 1000)

(defun sleepy-time ()
  (+ *min-sleep* (random (- *max-sleep* *min-sleep*))))

(defun prng-oracle ()
  (sleep (sleepy-time))
  (let* ((seed (get-universal-time))
         (rng (seed-prng (make-prng seed))))
    (sleep (sleepy-time))
    (format t "Seed was: ~A" seed)
    (extract-number rng)))

(let ((*min-sleep* 5)
      (*max-sleep* 15))
  (defun ex22 ()
    (let ((before (get-universal-time))
          (oracle-says (prng-oracle))
          (after (get-universal-time)))
      (loop for seed from before to after
            when (= oracle-says (extract-number (seed-prng (make-prng seed))))
              do (return-from ex22 (print seed))))))





