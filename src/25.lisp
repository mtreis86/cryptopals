(defun %mod-expt-original (a n m)
   (loop with c = 1 while (plusp n) do
      (if (oddp n) (setf c (mod (* a c) m)))
      (setf n (ash n -1))
      (setf a (mod (* a a) m))
      finally (return c)))
#|

Ok so this works but is still stupido. At least it doesn't have side effects anymore
Anyways there is a better way to do this, be explicit about what we're doing here.

Basically we are chunking up the exponent into bits,
so why aren't we just using ldb in the first place?
|#


(defun mod-expt (base expt mod)
  "Calculate the result if raising the base to the exponent then mod'ing."
  (loop for pos from 0 below (integer-length expt)
        for n = (ldb (byte 1 pos) expt)
        for tmp = base then (mod (* tmp tmp) mod)
        with result = 1
        unless (zerop n)
          do (setf result (mod (* tmp result) mod))
        finally (return result)))



