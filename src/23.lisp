(defpackage :ex23
  (:use :cl
        :cryptutils/prng
        :bit-foolery))

(in-package :ex23)

;;;; Clone an MT19937 RNG from its output

#|The internal state of MT19937 consists of 624 32 bit integers.

For each batch of 624 outputs, MT permutes that internal state. By permuting state regularly,
MT19937 achieves a period of 2**19937, which is Big.

Each time MT19937 is tapped, an element of its internal state is subjected to a tempering
function that diffuses bits through the result.

The tempering function is invertible; you can write an "untemper" function that takes an MT19937
output and transforms it back into the corresponding element of the MT19937 state array.

To invert the temper transform, apply the inverse of each of the operations in the temper transform
in reverse order. There are two kinds of operations in the temper transform each applied twice; one
is an XOR against a right-shifted value, and the other is an XOR against a left-shifted value AND'd
with a magic number. So you'll need code to invert the "right" and the "left" operation.

Once you have "untemper" working, create a new MT19937 generator, tap it for 624 outputs, untemper
each of them to recreate the state of the generator, and splice that state into a new instance of
the MT19937 generator.

The new "spliced" generator should predict the values of the original.
|#


(defvar *size* 32)
(defvar *mask-1* #xFFFFFFFF)
(defvar *mask-2* #x9D2C5680)
(defvar *mask-3* #xEFC60000)
(defvar *shift-1* 11)
(defvar *shift-2* 7)
(defvar *shift-3* 15)
(defvar *shift-4* 18)

(defun ex23 ()
  (let* ((prng (seed-prng (make-prng 100)))
         (first-tap (untemper (extract-number prng)))
         (spliced (cryptutils/prng::%make-prng
                   :seeded t
                   :seed first-tap
                   :index 0)))
    ;; manual seeding based on untempering
    (loop initially (setf (aref (cryptutils/prng::prng-array spliced) 0) first-tap)
          for index from 1 below 624
          do (let ((tap (extract-number prng)))
               (setf (aref (cryptutils/prng::prng-array spliced) index)
                     (untemper tap))))
    ;; reset prng
    (setf (cryptutils/prng::prng-index prng) 0)
    ;; test to confirm attack works
    (loop for index from 0 to 1000
          for spliced-result = (extract-number spliced)
          for prng-result = (extract-number prng)
          unless (eq spliced-result prng-result)
            do (error "splice not equal to prng: ~A to ~A." spliced-result prng-result)
          finally (return 'passed))))

(defun temper (num)
   (shift-xor
     (shift-and-xor
       (shift-and-xor
         (shift-xor
           num
          *shift-1* *size*)
        *shift-2* *size* *mask-2*)
      *shift-3* *size* *mask-3*)
    *shift-4* *size*))

(defun shift-and-xor (num shift frame-width constant)
  (logxor num (logand (shift-high num frame-width shift)
                      constant)))

(defun shift-xor (num shift frame-width)
  (logxor num (shift-low num frame-width shift)))

(defun untemper (num)
   (undo-shift-xor
     (undo-shift-and-xor
       (undo-shift-and-xor
         (undo-shift-xor
           num
          *shift-4* *size*)
        *shift-3* *size* *mask-3*)
      *shift-2* *size* *mask-2*)
    *shift-1* *size*))

(defun undo-shift-xor (num shift frame-width)
 (loop with init-pos = (- frame-width shift)
       with result = (shift-high (ldb (byte shift init-pos) num)
                                 frame-width init-pos)
       for end downfrom init-pos to 0 by shift
       for start = (max (- end shift) 0)
       for width = (- end start)
       for prev = (ldb (byte width (+ start shift)) result)
       do (let ((next (logxor prev (ldb (byte width start) num))))
            (setf result (dpb (ldb (byte width 0) next)
                              (byte width start)
                              result)))
       finally (return result)))

(defun undo-shift-and-xor (num shift frame-width constant)
  (loop with result = (ldb (byte shift 0) num)
        for start from 0 by shift
        for end from shift to frame-width by shift
        for prev = (ldb (byte shift start) result)
        do (let* ((next (shift-high prev frame-width end))
                  (next (logand next constant)))
             (setf result (dpb (ldb (byte shift end)
                                    (logxor next num))
                               (byte shift end)
                               result)))
        finally (return result)))

(defun test (num)
  (print-binary num) (terpri)
  (let ((shifted-1 (shift-xor num *shift-1* *size*)))
    (print-binary shifted-1)
    (let ((result-1 (undo-shift-xor shifted-1 *shift-1* *size*)))
      (print-binary result-1)
      (format t "Result of step is: ~A~%~%" (= num result-1))

      (let ((shifted-2 (shift-and-xor shifted-1 *shift-2* *size* *mask-2*)))
        (print-binary shifted-2)
        (let ((result-2 (undo-shift-and-xor shifted-2 *shift-2* *size* *mask-2*)))
          (print-binary result-2)
          (format t "Result of step is: ~A~%~%" (= shifted-1 result-2))

          (let ((shifted-3 (shift-and-xor shifted-2 *shift-3* *size* *mask-3*)))
            (print-binary shifted-3)
            (let ((result-3 (undo-shift-and-xor shifted-3 *shift-3* *size* *mask-3*)))
              (print-binary result-3)
              (format t "Result of step is: ~A~%~%" (= shifted-2 result-3))

              (let ((shifted-4 (shift-xor shifted-3 *shift-4* *size*)))
                (print-binary shifted-4)
                (let ((result-4 (undo-shift-xor shifted-4 *shift-4* *size*)))
                  (print-binary result-4)
                  (format t "Result of step is: ~A~%~%" (= shifted-3 result-4))
                  (and (= num result-1)
                       (= shifted-1 result-2)
                       (= shifted-2 result-3)
                       (= shifted-3 result-4)))))))))))
