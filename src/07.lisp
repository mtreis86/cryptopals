(in-package :cryptopals)



;;; AES in ECB mode
#| The Base64-encoded content in this file has been encrypted via AES-128 in ECB mode under the key
"YELLOW SUBMARINE".
case-sensitive, without the quotes; exactly 16 characters; I like "YELLOW SUBMARINE" because it's
exactly 16 bytes long, and now you do too.
Decrypt it. You know the key, after all.
Easiest way: use OpenSSL::Cipher and give it AES-128-ECB as the cipher. Do this with code.
You can obviously decrypt this using the OpenSSL command-line tool, but we're having you get ECB
working in code for a reason. You'll need it a lot later on, and not just for attacking ECB.|#


(defun ex07 ()
  (let* ((key "YELLOW SUBMARINE")
         (path #P"~/common-lisp/puzzles/cryptopals/encrypted-files/7.txt")
         (input (read-b64-file-to-byte-vect path))
         (desired-output *funky-music*))
    (string= (byte-vect-to-string (decrypt-aes-ecb input (string-to-byte-vect key)))
             desired-output)))
