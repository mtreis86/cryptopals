(defpackage ex19
  (:use :cl
        :cryptutils/prng
        :bit-foolery))

(in-package :ex19)

#|

TODO make this work again
I have no idea what I did to break this
Should have documented how it worked when it was fresh in my mind.
Too late now, I'll have to revisit at some point. For the time being this puzzle fails.

|#

;;;Break fixed-nonce CTR mode using substitutions
#| Take your CTR encrypt/decrypt function and fix its nonce value to 0. Generate a random AES key.

In successive encryptions (not in one big running CTR stream), encrypt each line of the base64
decodes of the following, producing multiple independent ciphertexts:

SSBoYXZlIG1ldCB0aGVtIGF0IGNsb3NlIG9mIGRheQ==
Q29taW5nIHdpdGggdml2aWQgZmFjZXM=
RnJvbSBjb3VudGVyIG9yIGRlc2sgYW1vbmcgZ3JleQ==
RWlnaHRlZW50aC1jZW50dXJ5IGhvdXNlcy4=
SSBoYXZlIHBhc3NlZCB3aXRoIGEgbm9kIG9mIHRoZSBoZWFk
T3IgcG9saXRlIG1lYW5pbmdsZXNzIHdvcmRzLA==
T3IgaGF2ZSBsaW5nZXJlZCBhd2hpbGUgYW5kIHNhaWQ=
UG9saXRlIG1lYW5pbmdsZXNzIHdvcmRzLA==
QW5kIHRob3VnaHQgYmVmb3JlIEkgaGFkIGRvbmU=
T2YgYSBtb2NraW5nIHRhbGUgb3IgYSBnaWJl
VG8gcGxlYXNlIGEgY29tcGFuaW9u
QXJvdW5kIHRoZSBmaXJlIGF0IHRoZSBjbHViLA==
QmVpbmcgY2VydGFpbiB0aGF0IHRoZXkgYW5kIEk=
QnV0IGxpdmVkIHdoZXJlIG1vdGxleSBpcyB3b3JuOg==
QWxsIGNoYW5nZWQsIGNoYW5nZWQgdXR0ZXJseTo=
QSB0ZXJyaWJsZSBiZWF1dHkgaXMgYm9ybi4=
VGhhdCB3b21hbidzIGRheXMgd2VyZSBzcGVudA==
SW4gaWdub3JhbnQgZ29vZCB3aWxsLA==
SGVyIG5pZ2h0cyBpbiBhcmd1bWVudA==
VW50aWwgaGVyIHZvaWNlIGdyZXcgc2hyaWxsLg==
V2hhdCB2b2ljZSBtb3JlIHN3ZWV0IHRoYW4gaGVycw==
V2hlbiB5b3VuZyBhbmQgYmVhdXRpZnVsLA==
U2hlIHJvZGUgdG8gaGFycmllcnM/
VGhpcyBtYW4gaGFkIGtlcHQgYSBzY2hvb2w=
QW5kIHJvZGUgb3VyIHdpbmdlZCBob3JzZS4=
VGhpcyBvdGhlciBoaXMgaGVscGVyIGFuZCBmcmllbmQ=
V2FzIGNvbWluZyBpbnRvIGhpcyBmb3JjZTs=
SGUgbWlnaHQgaGF2ZSB3b24gZmFtZSBpbiB0aGUgZW5kLA==
U28gc2Vuc2l0aXZlIGhpcyBuYXR1cmUgc2VlbWVkLA==
U28gZGFyaW5nIGFuZCBzd2VldCBoaXMgdGhvdWdodC4=
VGhpcyBvdGhlciBtYW4gSSBoYWQgZHJlYW1lZA==
QSBkcnVua2VuLCB2YWluLWdsb3Jpb3VzIGxvdXQu
SGUgaGFkIGRvbmUgbW9zdCBiaXR0ZXIgd3Jvbmc=
VG8gc29tZSB3aG8gYXJlIG5lYXIgbXkgaGVhcnQs
WWV0IEkgbnVtYmVyIGhpbSBpbiB0aGUgc29uZzs=
SGUsIHRvbywgaGFzIHJlc2lnbmVkIGhpcyBwYXJ0
SW4gdGhlIGNhc3VhbCBjb21lZHk7
SGUsIHRvbywgaGFzIGJlZW4gY2hhbmdlZCBpbiBoaXMgdHVybiw=
VHJhbnNmb3JtZWQgdXR0ZXJseTo=
QSB0ZXJyaWJsZSBiZWF1dHkgaXMgYm9ybi4=

(This should produce 40 short CTR-encrypted ciphertexts).

Because the CTR nonce wasn't randomized for each encryption, each ciphertext has been encrypted
against the same keystream. This is very bad.

Understanding that, like most stream ciphers including RC4, and obviously any block cipher run in
CTR mode, the actual "encryption" of a byte of data boils down to a single XOR operation, it should
be plain that:

CIPHERTEXT-BYTE XOR PLAINTEXT-BYTE = KEYSTREAM-BYTE

And since the keystream is the same for every ciphertext:

CIPHERTEXT-BYTE XOR KEYSTREAM-BYTE = PLAINTEXT-BYTE ie, "you don't say!"

Attack this cryptosystem piecemeal: guess letters, use expected English language frequence to
validate guesses, catch common English trigrams, and so on. Don't overthink it. Points for
automating this, but part of the reason I'm having you do this is that I think this approach is
suboptimal.|#

;; (let ((bytes (loop for byte from 0 to 255 collecting byte))
;;       (v1 #(1 2 3))
;;       (v2 #(6 7 8))
;;       (whitelist '(1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16)))
;;   (amb ((a bytes :shufflep t)
;;         (a1 whitelist)
;;         (a2 whitelist))
;;     (constrain (= a1 (logxor a (aref v1 0))))
;;     (constrain (= a2 (logxor a (aref v2 0))))
;;     (amb ((b bytes :shufflep t)
;;           (b1 whitelist)
;;           (b2 whitelist))
;;       (constrain (= b1 (logxor b (aref v1 1))))
;;       (constrain (= b2 (logxor b (aref v2 1))))
;;       (amb ((c bytes :shufflep t)
;;             (c1 whitelist)
;;             (c2 whitelist))
;;         (constrain (= c1 (logxor c (aref v1 2))))
;;         (constrain (= c2 (logxor c (aref v2 2))))
;;         (list a b c)))))


;; (defun test ()
;;  (let ((bytes (loop for byte from 0 to 255 collecting byte))
;;        (whitelist (loop for num from 1 to 16 collecting num)))
;;    (bind-amb (#(1 2 3) #(6 7 8)) whitelist bytes)))

;; (defmacro bind-amb (vect-list whitelist bytes &optional (index 0) (key-list '()))
;;   (gen-amb vect-list whitelist bytes index key-list))
#|
(defmacro bind-amb (vect-list whitelist bytes &optional (index 0))
  (gen-amb vect-list whitelist bytes index))

(defun gen-amb (vect-list whitelist bytes index)
  (labels ((gen-constraint (plain key-char vect index)
             `(constrain (= ,plain (logxor ,key-char (aref ,vect ,index)))))
           (gen-binding (plain)
             `(,plain whitelist))
           (gen-vect-syms ()
             (let (vect-bindings)
               (dolist (vect vect-list)
                 (let ((sym (gensym "VECT")))
                   (push (list sym vect) vect-bindings)))
               (nreverse vect-bindings)))
           (next-vect-syms (vect-syms index)
             (let (next-syms)
               (dolist (vect-sym-pair vect-syms)
                 (when (and (< index 16)
                            (> (array-dimension (first (rest vect-sym-pair)) 0) index))
                   (push vect-sym-pair next-syms)))
               (nreverse next-syms)))
           (next-index (index vect-syms)
             (let* ((key-char (gensym "KEY"))
                    (nested (gen-nested key-char index vect-syms))
                    (next-vect-syms (next-vect-syms vect-syms (1+ index)))
                    (next-amb (when next-vect-syms (next-index (1+ index) next-vect-syms))))
               `((let (key-maybe)
                  (amb ((,key-char bytes)
                        (:signalp nil))
                    ,nested
                    nil)
                  (push key-maybe key-syms))
                 ,@next-amb)))
           (gen-nested (key-char index vect-syms)
             (let* ((plain (gensym "PLAIN"))
                    (binding (gen-binding plain))
                    (vect-pair (first vect-syms))
                    (vect (first vect-pair))
                    (constraint (gen-constraint plain key-char vect index))
                    (next-nested (if (rest vect-syms)
                                     (gen-nested key-char index (rest vect-syms))
                                     `(push ,key-char key-maybe))))
               `(amb (,binding)
                  ,constraint
                  ,next-nested))))
    (let* ((vect-syms (gen-vect-syms))
           (amb (next-index index vect-syms)))
      `(let ((whitelist ,whitelist)
             (bytes ,bytes)
             (key-syms nil)
             ,@vect-syms)
         ,@amb
         (nreverse key-syms)))))
|#
#|
(defun gen-amb (vect-list whitelist bytes index key-list)
  (labels ((gen-constraints (plain key-char vect index)
             `(constrain (= ,plain (logxor ,key-char (aref ,vect ,index)))))
           (gen-bindings (plain)
             `(,plain whitelist))
           (next-amb (vect-bindings index key-list)
             (let ((constraints nil) (bindings nil) (next-vect-list nil)
                   (key-char (gensym "KEY"))
                   (next-index (1+ index)))
               (dolist (vect-binding-pair vect-bindings)
                 (let ((plain (gensym "PLAIN"))
                       (sym (first vect-binding-pair))
                       (vect (first (rest vect-binding-pair))))
                   (when (> (array-dimension vect 0) index)
                     (push (gen-constraints plain key-char sym index) constraints)
                     (push (gen-bindings plain) bindings)
                     (push (list sym vect) next-vect-list))))
               (if (< (length next-vect-list) 2)
                   (let ((key-list (nreverse key-list)))
                     `(list ,@key-list))
                   (let* ((key-list (push key-char key-list))
                          (next-vect-list (nreverse next-vect-list))
                          (next-amb (next-amb next-vect-list next-index key-list))
                          (constraints (nreverse constraints))
                          (bindings (nreverse bindings)))
                     `(amb ((,key-char bytes)
                            ,@bindings)
                       ,@constraints
                        ,next-amb)))))
           (gen-vects ()
             (let (vect-bindings)
               (dolist (vect vect-list)
                 (let ((sym (gensym "VECT")))
                   (push (list sym vect) vect-bindings)))
               (nreverse vect-bindings))))
    (let* ((vect-bindings (gen-vects))
           (amb (next-amb vect-bindings index key-list)))
      `(let ((whitelist ,whitelist)
             (bytes ,bytes)
             ,@vect-bindings)
         ,amb))))
|#
#|
(defmacro attack-aes-ctr-list (ctr-list)
  "Assuming the ctr-list is all encodings that used the same nonce and key, and started with the
  counter at the same value, decode the list using char frequencies."
  (let ((bytes (loop for byte from 0 to 255 collecting byte))
        (whitelist (append *lowercase-codes* *uppercase-codes*
                           *whitespace-codes* *common-punctuation-codes*)))
    `(bind-amb ,ctr-list ',whitelist ',bytes)))
|#

(defmacro gen-ex19 ()
  (let* ((b64-encodes '("SSBoYXZlIG1ldCB0aGVtIGF0IGNsb3NlIG9mIGRheQ=="
                        "Q29taW5nIHdpdGggdml2aWQgZmFjZXM="))
                        ;; "RnJvbSBjb3VudGVyIG9yIGRlc2sgYW1vbmcgZ3JleQ=="
                        ;; "RWlnaHRlZW50aC1jZW50dXJ5IGhvdXNlcy4="
                        ;; "SSBoYXZlIHBhc3NlZCB3aXRoIGEgbm9kIG9mIHRoZSBoZWFk"
                        ;; "T3IgcG9saXRlIG1lYW5pbmdsZXNzIHdvcmRzLA=="
                        ;; "T3IgaGF2ZSBsaW5nZXJlZCBhd2hpbGUgYW5kIHNhaWQ="
                        ;; "UG9saXRlIG1lYW5pbmdsZXNzIHdvcmRzLA=="
                        ;; "QW5kIHRob3VnaHQgYmVmb3JlIEkgaGFkIGRvbmU="
                        ;; "T2YgYSBtb2NraW5nIHRhbGUgb3IgYSBnaWJl"
                        ;; "VG8gcGxlYXNlIGEgY29tcGFuaW9u"
                        ;; "QXJvdW5kIHRoZSBmaXJlIGF0IHRoZSBjbHViLA=="
                        ;; "QmVpbmcgY2VydGFpbiB0aGF0IHRoZXkgYW5kIEk="
                        ;; "QnV0IGxpdmVkIHdoZXJlIG1vdGxleSBpcyB3b3JuOg=="
                        ;; "QWxsIGNoYW5nZWQsIGNoYW5nZWQgdXR0ZXJseTo="
                        ;; "QSB0ZXJyaWJsZSBiZWF1dHkgaXMgYm9ybi4="))
                        ;; "VGhhdCB3b21hbidzIGRheXMgd2VyZSBzcGVudA=="
                        ;; "SW4gaWdub3JhbnQgZ29vZCB3aWxsLA=="
                        ;; "SGVyIG5pZ2h0cyBpbiBhcmd1bWVudA=="
                        ;; "VW50aWwgaGVyIHZvaWNlIGdyZXcgc2hyaWxsLg=="
                        ;; "V2hhdCB2b2ljZSBtb3JlIHN3ZWV0IHRoYW4gaGVycw=="
                        ;; "V2hlbiB5b3VuZyBhbmQgYmVhdXRpZnVsLA=="
                        ;; "U2hlIHJvZGUgdG8gaGFycmllcnM/"
                        ;; "VGhpcyBtYW4gaGFkIGtlcHQgYSBzY2hvb2w="
                        ;; "QW5kIHJvZGUgb3VyIHdpbmdlZCBob3JzZS4="
                        ;; "VGhpcyBvdGhlciBoaXMgaGVscGVyIGFuZCBmcmllbmQ="
                        ;; "V2FzIGNvbWluZyBpbnRvIGhpcyBmb3JjZTs="
                        ;; "SGUgbWlnaHQgaGF2ZSB3b24gZmFtZSBpbiB0aGUgZW5kLA=="
                        ;; "U28gc2Vuc2l0aXZlIGhpcyBuYXR1cmUgc2VlbWVkLA=="
                        ;; "U28gZGFyaW5nIGFuZCBzd2VldCBoaXMgdGhvdWdodC4="
                        ;; "VGhpcyBvdGhlciBtYW4gSSBoYWQgZHJlYW1lZA=="
                        ;; "QSBkcnVua2VuLCB2YWluLWdsb3Jpb3VzIGxvdXQu"
                        ;; "SGUgaGFkIGRvbmUgbW9zdCBiaXR0ZXIgd3Jvbmc="
                        ;; "VG8gc29tZSB3aG8gYXJlIG5lYXIgbXkgaGVhcnQs"
                        ;; "WWV0IEkgbnVtYmVyIGhpbSBpbiB0aGUgc29uZzs="
                        ;; "SGUsIHRvbywgaGFzIHJlc2lnbmVkIGhpcyBwYXJ0"
                        ;; "SW4gdGhlIGNhc3VhbCBjb21lZHk7"
                        ;; "SGUsIHRvbywgaGFzIGJlZW4gY2hhbmdlZCBpbiBoaXMgdHVybiw="
                        ;; "VHJhbnNmb3JtZWQgdXR0ZXJseTo="))
         (decodes (loop for encode in b64-encodes collecting (b64-string-to-byte-vect encode)))
         (block-size 16)
         (nonce (make-random-byte-vect (/ block-size 2)))
         (key (make-random-byte-vect block-size))
         (ctr-encodes (loop for decode in decodes collecting (encrypt-aes-ctr decode key nonce))))
    (print (encrypt-aes-ecb (byte-vect-list-to-byte-vect (list nonce (make-byte-vect 8)))
                            key))
    `(attack-aes-ctr-list ,ctr-encodes)))

(defun ex19 ()
  nil)

;; (defun attack-aes-ctr-list (ctr-list)
;;   "Assuming the ctr-list is all encodings that used the same nonce and key, and started with the
;;   counter at the same value, decode the list using char frequencies."
;;   (attempt-decode-xor-against-repeat-known-block-size ctr-list))
#|


(defun ex19 ()
  (let* ((b64-plaintexts '("SSBoYXZlIG1ldCB0aGVtIGF0IGNsb3NlIG9mIGRheQ=="
                           "Q29taW5nIHdpdGggdml2aWQgZmFjZXM="
                           "RnJvbSBjb3VudGVyIG9yIGRlc2sgYW1vbmcgZ3JleQ=="
                           "RWlnaHRlZW50aC1jZW50dXJ5IGhvdXNlcy4="
                           "SSBoYXZlIHBhc3NlZCB3aXRoIGEgbm9kIG9mIHRoZSBoZWFk"
                           "T3IgcG9saXRlIG1lYW5pbmdsZXNzIHdvcmRzLA=="
                           "T3IgaGF2ZSBsaW5nZXJlZCBhd2hpbGUgYW5kIHNhaWQ="
                           "UG9saXRlIG1lYW5pbmdsZXNzIHdvcmRzLA=="
                           "QW5kIHRob3VnaHQgYmVmb3JlIEkgaGFkIGRvbmU="
                           "T2YgYSBtb2NraW5nIHRhbGUgb3IgYSBnaWJl"
                           "VG8gcGxlYXNlIGEgY29tcGFuaW9u"
                           "QXJvdW5kIHRoZSBmaXJlIGF0IHRoZSBjbHViLA=="
                           "QmVpbmcgY2VydGFpbiB0aGF0IHRoZXkgYW5kIEk="
                           "QnV0IGxpdmVkIHdoZXJlIG1vdGxleSBpcyB3b3JuOg=="
                           "QWxsIGNoYW5nZWQsIGNoYW5nZWQgdXR0ZXJseTo="
                           "QSB0ZXJyaWJsZSBiZWF1dHkgaXMgYm9ybi4="
                           "VGhhdCB3b21hbidzIGRheXMgd2VyZSBzcGVudA=="
                           "SW4gaWdub3JhbnQgZ29vZCB3aWxsLA=="
                           "SGVyIG5pZ2h0cyBpbiBhcmd1bWVudA=="
                           "VW50aWwgaGVyIHZvaWNlIGdyZXcgc2hyaWxsLg=="
                           "V2hhdCB2b2ljZSBtb3JlIHN3ZWV0IHRoYW4gaGVycw=="
                           "V2hlbiB5b3VuZyBhbmQgYmVhdXRpZnVsLA=="
                           "U2hlIHJvZGUgdG8gaGFycmllcnM/"
                           "VGhpcyBtYW4gaGFkIGtlcHQgYSBzY2hvb2w="
                           "QW5kIHJvZGUgb3VyIHdpbmdlZCBob3JzZS4="
                           "VGhpcyBvdGhlciBoaXMgaGVscGVyIGFuZCBmcmllbmQ="
                           "V2FzIGNvbWluZyBpbnRvIGhpcyBmb3JjZTs="
                           "SGUgbWlnaHQgaGF2ZSB3b24gZmFtZSBpbiB0aGUgZW5kLA=="
                           "U28gc2Vuc2l0aXZlIGhpcyBuYXR1cmUgc2VlbWVkLA=="
                           "U28gZGFyaW5nIGFuZCBzd2VldCBoaXMgdGhvdWdodC4="
                           "VGhpcyBvdGhlciBtYW4gSSBoYWQgZHJlYW1lZA=="
                           "QSBkcnVua2VuLCB2YWluLWdsb3Jpb3VzIGxvdXQu"
                           "SGUgaGFkIGRvbmUgbW9zdCBiaXR0ZXIgd3Jvbmc="
                           "VG8gc29tZSB3aG8gYXJlIG5lYXIgbXkgaGVhcnQs"
                           "WWV0IEkgbnVtYmVyIGhpbSBpbiB0aGUgc29uZzs="
                           "SGUsIHRvbywgaGFzIHJlc2lnbmVkIGhpcyBwYXJ0"
                           "SW4gdGhlIGNhc3VhbCBjb21lZHk7"
                           "SGUsIHRvbywgaGFzIGJlZW4gY2hhbmdlZCBpbiBoaXMgdHVybiw="
                           "VHJhbnNmb3JtZWQgdXR0ZXJseTo="))
         (plaintexts (loop for b64 in b64-plaintexts collecting (b64-string-to-byte-vect b64)))
         (block-size 16)
         (nonce (make-random-byte-vect (/ block-size 2)))
         (key (make-random-byte-vect block-size))
         (ctr-encodes (loop for text in plaintexts collecting (encrypt-aes-ctr text key nonce))))
    (print (encrypt-aes-ecb (byte-vect-list-to-byte-vect (list nonce (make-byte-vect 8)))
                            key))
    (initialize-grams)
    (all-english-producing-digrams ctr-encodes (print (all-english-producing-keys ctr-encodes)))))

(defvar *maybe-english* (append *uppercase-codes* *lowercase-codes* *common-punctuation-codes*
                                *whitespace-codes* *digit-codes*))

(defun make-bit-vect-from-char-codes (code-list)
  (let ((vect (make-array 256 :element-type 'bit :initial-element 0
                              :fill-pointer nil :adjustable nil)))
    (loop for entry in code-list do (setf (aref vect entry) 1))
    vect))

(defvar *maybe-english-bit-vect* (make-bit-vect-from-char-codes *maybe-english*))

(defun all-english-producing-keys (vect-list)
  "When the vects in the list get xored against the same key, which of the possible keys will
  produce english plaintext? Returns a list of lists, the first entry in each list is the index,
  the rest is potential bytes in the key at that index."
  (let ((max (max-length vect-list)))
    (loop for index from 0 below max
          collecting
          (cons index
                (loop for key from 0 below 255
                      when (loop for vect in vect-list
                                 when (and (< index (length vect))
                                           (zerop (aref *maybe-english-bit-vect*
                                                        (logxor key (aref vect index)))))
                                  do (return nil)
                                 finally (return t))
                       collecting key)))))

(defun all-english-producing-digrams (vect-list key-bytes-list)
  "When the vects in the vect-list get xored against the same key, which key combinations of the
  possible keys in the key-bytes-list can product valid english digrams?"
  (loop with results = nil
        for (key-index . key-bytes) in key-bytes-list
        for (next-index . next-bytes) in (rest key-bytes-list)
        for key-result = nil
        do (assert (= next-index (1+ key-index)))
           (loop for key in key-bytes
                 do (loop for next in next-bytes
                          do (loop for vect in vect-list
                                   when (< next-index (length vect))
                                     do (let* ((key-byte (aref vect key-index))
                                               (key-xor (logxor key key-byte))
                                               (next-byte (aref vect next-index))
                                               (next-xor (logxor next next-byte)))
                                          (unless (lookup-gram (list key-xor next-xor))
                                            (return)))
                                   finally (pushnew key key-result))))
           (setf key-result (nreverse key-result))
           (push key-index key-result)
           (push key-result results)
        finally (return (nreverse results))))



|#



