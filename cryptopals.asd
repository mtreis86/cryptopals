(defsystem "cryptopals"
  :version "0.1.0"
  :author ""
  :license ""
  :depends-on ("cryptutils")
  :components ((:module "src"
                :components
                ((:file "packages")
                 (:file "main")
                 (:file "01")
                 (:file "02")
                 (:file "03")
                 (:file "04")
                 (:file "05")
                 (:file "06")
                 (:file "07")
                 (:file "08")
                 (:file "09")
                 (:file "10")
                 (:file "11")
                 (:file "12")
                 (:file "13")
                 (:file "14")
                 (:file "15")
                 (:file "16")
                 (:file "17")
                 (:file "18")
                 (:file "19")
                 (:file "20")
                 (:file "21")
                 (:file "22")
                 (:file "23")
                 (:file "24")
                 (:file "25"))))
  :description ""
  :in-order-to ((test-op (test-op "cryptopals/tests"))))

(defsystem "cryptopals/tests"
  :author ""
  :license ""
  :depends-on ("cryptopals"
               "rove")
  :components ((:module "tests"
                :components
                ((:file "main"))))
  :description "Test system for cryptopals"
  :perform (test-op (op c) (symbol-call :rove :run c)))
